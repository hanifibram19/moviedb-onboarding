package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.interactor.UseCase
import javax.inject.Inject

class GetMovies
@Inject constructor(private val movies2Repository: MoviesRepository) : UseCase<MovieWrapper, UseCase.None>() {
    override suspend fun run(params: None) = movies2Repository.getPopularMovies()
}