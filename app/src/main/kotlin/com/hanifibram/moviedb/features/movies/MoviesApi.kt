package com.hanifibram.moviedb.features.movies

import retrofit2.Call
import retrofit2.http.GET

internal interface MoviesApi {

    @GET("movie/popular")
    fun getPopularMovie(): Call<MovieWrapper>

}