package com.hanifibram.moviedb.features.movies

import android.os.Bundle
import android.view.View
import com.hanifibram.moviedb.R
import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.extension.failure
import com.hanifibram.moviedb.core.extension.observe
import com.hanifibram.moviedb.core.extension.viewModel
import com.hanifibram.moviedb.core.navigation.Navigator
import com.hanifibram.moviedb.core.platform.BaseFragment
import javax.inject.Inject

class MoviesFragment : BaseFragment() {

    @Inject
    lateinit var navigator: Navigator
    @Inject
    lateinit var moviesViewModel: MoviesViewModel

    override fun layoutId() = R.layout.fragment_movies

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        moviesViewModel = viewModel(viewModelFactory) {
            observe(movies, ::renderMoviesList)
            failure(failure, ::handleFailure)
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        moviesViewModel.loadMovie()
    }

    private fun renderMoviesList(movies: List<Movie>?) {
    }

    private fun handleFailure(failure: Failure?) {
    }

}