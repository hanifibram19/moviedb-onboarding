package com.hanifibram.moviedb.features.movies

import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesService
@Inject constructor(retrofit: Retrofit) : MoviesApi {
    private val movies2Api by lazy { retrofit.create(MoviesApi::class.java) }

    override fun getPopularMovie(): Call<MovieWrapper> = movies2Api.getPopularMovie()
}