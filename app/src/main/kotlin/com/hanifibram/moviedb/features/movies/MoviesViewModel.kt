package com.hanifibram.moviedb.features.movies

import android.arch.lifecycle.MutableLiveData
import com.hanifibram.moviedb.core.interactor.UseCase
import com.hanifibram.moviedb.core.platform.BaseViewModel
import javax.inject.Inject

class MoviesViewModel
@Inject constructor(private val getMovies2: GetMovies) : BaseViewModel() {

    var movies: MutableLiveData<List<Movie>> = MutableLiveData()

    fun loadMovie() = getMovies2(UseCase.None()) { it.fold(::handleFailure, ::handleMovieList) }

    private fun handleMovieList(movie: MovieWrapper) {
        this.movies.value = movie.results
    }

}